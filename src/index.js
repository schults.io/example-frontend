import loader from './loader';

(async function() {
    const [
        svelteTest,
        header
    ] = await Promise.all([
        loader.load('svelte:test'),
        loader.load('header'),
        loader.load('example:components'),
    ]);
    
    header({ target: document.querySelector('#header') });
    svelteTest({ target: document.querySelector('#svelte-root') });
})()
