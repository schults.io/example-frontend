function loadJS(url) {
    return new Promise((resolve, reject) => {
        var script = document.createElement("script")
        script.type = "text/javascript";

        script.onload = resolve;
        script.onerror = reject;

        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    })
}

function loadCSS(url) {
    return new Promise((resolve, reject) => {
        let link = document.createElement("link");
        link.type = "text/css";
        link.rel = "stylesheet";
        link.onload = resolve;
        link.onerror = reject;
        link.href = url;
        document.getElementsByTagName("head")[0].appendChild(link);
    });
}

const componentConfig = {
    'example:components': { js: '/example-frontend/components.js' },
    'example:data': { js: '/example-frontend/data.js' },
    'svelte:test': {
        js: '/example-frontend-test/bundle.js',
        css: '/example-frontend-test/bundle.css',
    },
    'header': {
        js: '/example-frontend-header/bundle.js',
        css: '/example-frontend-header/bundle.css',
    },
};

window.loader = (function () {
    const cache = {};

    return {
        load(component) {
            if (!cache[component]) {
                const config = componentConfig[component];

                if (!config) {
                    throw new Error('ArgumentException: Not supported "' + component + '"');
                }

                cache[component] = loadJS(config.js).then(() => {
                    const value = window[component](window.loader);

                    delete window[component];

                    return value;
                });

                config.css && loadCSS(config.css);
            }

            return cache[component];
        }
    }
}())

export default window.loader;
