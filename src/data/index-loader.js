export default async function (loader) {
    const subs = {};

    function unsub(model, fn) {
        subs[model].splice(subs[model].indexOf(fn), 1)
    }
    const models = {
        profile: {
            name: 'Frankenstein'
        }
    }

    const wrappedModels = {};

    Object.keys(models).forEach(key => {
        models[key].subscribe = (cb) => {
            cb(models[key]);
            subs[key] = (subs[key] || []).concat(cb);
            return () => unsub(model, cb)
        }
        models[key].unsub = (cb) => {
            unsub(key, cb)
        }

        wrappedModels[key] = new Proxy(models[key], {
            set: function(obj, prop, value) {
                obj[prop] = value;
                subs[key].forEach(sub => sub(models.profile));
                return true;
            }
        })
    });

    return wrappedModels;
}