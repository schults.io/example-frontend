import { html, LitElement, property, customElement } from 'lit-element';

@customElement('name-displayer')
class MyElement extends LitElement {
  @property({ type: String })
  myProp = 'stuff';

  connectedCallback() {
    super.connectedCallback();

    window.loader.load('example:data').then(data => {
      this.cleanup = data.profile.subscribe((profile) => {
        this.name = profile.name;
        this.update()
      });;
    })
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.cleanup && this.cleanup();
  }

  render() {
    return html`
      <p>Hello from web component: ${this.name}</p>
    `;
  }
}