const html = require('@rollup/plugin-html');
const fs = require('fs');
const resolve = require('rollup-plugin-node-resolve');
const babel = require('rollup-plugin-babel');

export default [
  {
    input: 'src/index.js',
    output: {
      dir: 'dist',
      format: 'iife'
    },
    plugins: [html({
      template() {
        return fs.readFileSync(__dirname + '/src/index.html')
      }
    })]
  },
  {
    input: 'src/components/index-loader.js',
    output: {
      name: 'example:components',
      file: 'dist/components.js',
      format: 'umd'
    },
    plugins: [
      resolve(),
      babel()
    ]
  },
  {
    input: 'src/data/index-loader.js',
    output: {
      name: 'example:data',
      file: 'dist/data.js',
      format: 'umd'
    }
  }
]