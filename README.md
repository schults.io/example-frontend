# example-fe

Playground for loading components from external sources like [example-frontend-header](https://gitlab.com/schults.io/example-frontend-header/) or [example-frontend-test](https://gitlab.com/schults.io/example-frontend-test/).

Visit here - [https://www.schults.ee/example-frontend/](https://www.schults.ee/example-frontend/)
